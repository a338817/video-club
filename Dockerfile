FROM node 
MAINTAINER Eric Aguilar MARCIAL
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start